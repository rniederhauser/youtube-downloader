from pytube import YouTube
import os
from tkinter import messagebox
from tkinter import *

def get_download_path():
    """Returns the default downloads path for linux or windows"""
    if os.name == 'nt':
        import winreg
        sub_key = r'SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders'
        downloads_guid = '{374DE290-123F-4565-9164-39C4925E467B}'
        with winreg.OpenKey(winreg.HKEY_CURRENT_USER, sub_key) as key:
            location = winreg.QueryValueEx(key, downloads_guid)[0]
        return location
    else:
        return os.path.join(os.path.expanduser('~'), 'downloads')

def downloadVideo():
    link = E1.get()
    yt = YouTube(link)

    try: 
        #downloading the video
        yt.streams.filter(progressive=True, file_extension='mp4').order_by('resolution').desc().first().download(get_download_path())
    except: 
        messagebox.showerror("Error", "Video couldn't be loaded")
    messagebox.showinfo("Success", "Video was downloaded to Downloads folder")

root = Tk()
root.title("Youtube Video Downloader")
root.geometry("350x50")

frame = Frame(root)
frame.pack()

L1 = Label(frame, text="Videolink:")
L1.pack(side = LEFT)
E1 = Entry(frame)
E1.pack(side = LEFT)

B1 = Button(frame, text="Download", command=downloadVideo, fg="black")
B1.pack(side = LEFT)

root.mainloop()



